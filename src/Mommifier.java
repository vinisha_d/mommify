import java.util.List;

import static java.util.Arrays.asList;

public class Mommifier {
    public String mommify(String input) {
        double THIRTY_PERCENT = 0.3;
        StringBuilder output = new StringBuilder();
        List<String> vowels = asList("a", "e", "i", "o", "u");
        if (input.length() == 0 || percentageOfVowelsInInputString(input, vowels) < THIRTY_PERCENT) {
            return input;
        }
        else if(vowels.contains(input)){
            return "mommy";
        }
        return getMommifiedString(input, output, vowels);
    }

    private String getMommifiedString(String input, StringBuilder output, List<String> vowels) {
        boolean isVowel = false;
        for (int index = 0; index < input.length(); index++) {
            if (vowels.contains(String.valueOf(input.charAt(index)))) {
                isVowel = true;
                continue;
            }
            if (isVowel) {
                output.append(String.valueOf("mommy"));
            }
            output.append(String.valueOf(input.charAt(index)));
        }
        return String.valueOf(output);
    }

    private double percentageOfVowelsInInputString(String input, List<String> vowels) {
        double vowelsCount = 0;
        for (int position = 0; position < input.length(); position++) {
            if (vowels.contains(String.valueOf(input.charAt(position)))) {
                vowelsCount++;
            }
        }
        return vowelsCount / input.length();
    }
}
