import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MommifierTest {
    @Test
    public void shouldNotMommifyEmptyString() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("");
        assertEquals("", output);
    }

    @Test
    public void shouldNotMommifyConsonant() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("b");
        assertEquals("b", output);
    }

    @Test
    public void shouldMommifyVowelA() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("a");
        assertEquals("mommy", output);
    }

    @Test
    public void shouldMommifyVowelE() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("e");
        assertEquals("mommy", output);
    }

    @Test
    public void shouldMommifyVowelI() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("i");
        assertEquals("mommy", output);
    }

    @Test
    public void shouldMommifyVowelO() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("o");
        assertEquals("mommy", output);
    }

    @Test
    public void shouldMommifyVowelU() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("u");
        assertEquals("mommy", output);
    }

    @Test
    public void shouldNotMommifyConsonantString() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("bcd");
        assertEquals("bcd", output);
    }

    @Test
    public void shouldNotMommifyIfVowelIsLessThan30PercentOfInputStringLength() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("hard");
        assertEquals("hard", output);
    }

    @Test
    public void shouldMommifyIfVowelIsMoreThan30PercentOfInputStringLength() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("hear");
        assertEquals("hmommyr", output);
    }

    @Test
    public void shouldMommifyIfVowelIsMoreThan30PercentOfInputStringLengthAndNotConsecutive() {
        Mommifier mommifier = new Mommifier();
        String output = mommifier.mommify("had");
        assertEquals("hmommyd", output);
    }
}
